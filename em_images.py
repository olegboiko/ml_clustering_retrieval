import numpy as np
from em_func import *
from em import *
import pickle

data = read_images_data(os.path.abspath('data/em/images'))
K = 4 # number of clusters


def calculate_result(data, K):
    pickled_result = 'em_result.pickle'
    if os.path.isfile(pickled_result):
        with open(pickled_result, 'rb') as f:
            return pickle.load(f)
    else:
        np.random.seed(1)

        # Initalize parameters
        init_means = [data[x,:] for x in np.random.choice(len(data), K, replace=False)]
        cov = np.diag([data[:, 0].var(), data[:, 1].var(), data[:, 2].var()])
        init_covariances = [cov, cov, cov, cov]
        init_weights = [1/4., 1/4., 1/4., 1/4.]

        # Run our EM algorithm on the image data using the above initializations.
        # This should converge in about 125 iterations
        out = em(data, init_means, init_covariances, init_weights)
        with open('em_result.pickle', 'wb') as f:
            pickle.dump(out, f, pickle.HIGHEST_PROTOCOL)

        return out

out = calculate_result(data, K)

ll = out['loglik']
plt.plot(range(3,len(ll)),ll[3:],linewidth=4)
plt.xlabel('Iteration')
plt.ylabel('Log-likelihood')
plt.rcParams.update({'font.size':16})
# plt.tight_layout()
# plt.show()

# out = {'weights': weights, 'means': means, 'covs': covariances, 'loglik': ll_trace, 'resp': resp}
for k in range(K):
    r = out['weights'][k]*multivariate_normal.pdf(data[0,:], mean=out['means'][k], cov=out['covs'][k])
    print(r)