import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import pairwise_distances
from sklearn.preprocessing import normalize
from nn_features_and_metrics_func import *


def plot_heterogeneity(heterogeneity, k):
    plt.figure(figsize=(7, 4))
    plt.plot(heterogeneity, linewidth=4)
    plt.xlabel('# Iterations')
    plt.ylabel('Heterogeneity')
    plt.title('Heterogeneity of clustering over time, K={0:d}'.format(k))
    plt.rcParams.update({'font.size': 16})
    # plt.tight_layout()
    plt.show()


def get_initial_centroids(data, k, seed=None):
    if seed is not None:
        np.random.seed(seed)
    n = data.shape[0] #number of data points
    rand_indices = np.random.randint(0, n, k)
    return data[rand_indices, :].toarray()


def assign_clusters(data, centroids):
    dist = pairwise_distances(data, centroids, metric="euclidean")
    return np.argmin(dist, 1)


def revise_centroids(data, k, cluster_assignment):
    return np.array([np.mean(data[cluster_assignment == i], axis=0).A1 for i in range(k)])


def compute_heterogeneity(data, k, centroids, cluster_assignment):
    heterogeneity = 0.0
    for i in range(k):
        cluster_members = data[cluster_assignment == i]
        if cluster_members.shape[0] > 0:
            dist = pairwise_distances(cluster_members, [centroids[i]])
            heterogeneity += np.sum(dist ** 2)
    return heterogeneity


def kmeans(data, k, initial_centroids, maxiter=100, verbose=False, track_heterogeneity=False):
    heterogeneity_hist = []
    heterogeneity = None
    centroids = initial_centroids
    prev_cluster_assignments = None
    for i in range(maxiter):
        cluster_assignments = assign_clusters(data, centroids)
        if track_heterogeneity:
            current_heterogeneity = compute_heterogeneity(data, k, centroids, cluster_assignments)
            if verbose:
                print(current_heterogeneity)
            if heterogeneity is None or current_heterogeneity < heterogeneity:
                heterogeneity = current_heterogeneity
                heterogeneity_hist.append(heterogeneity)
            elif current_heterogeneity > heterogeneity:
                raise Exception('Clusterization is diverging')

        if prev_cluster_assignments is not None and \
                (cluster_assignments == prev_cluster_assignments).all():
            break
        else:
            prev_cluster_assignments = cluster_assignments
            centroids = revise_centroids(data, k, cluster_assignments)
    return [centroids, cluster_assignments, heterogeneity_hist, i]


def smart_initialize(data, k, seed=None):
    if seed is not None:
        np.random.seed(seed)

    n = data.shape[0]
    centroids = data[np.random.choice(n)].toarray()
    distances = pairwise_distances(data, centroids).flatten()**2

    for i in range(1, k):
        centroid = data[np.random.choice(n, p=distances/sum(distances))].toarray()
        centroids = np.append(centroids, centroid, axis=0)
        distances = np.min(pairwise_distances(data, centroids), axis=1)**2

    return centroids


def smart_initialize_2(data, k, seed=None):
    """Use k-means++ to initialize a good set of centroids"""
    if seed is not None:  # useful for obtaining consistent results
        np.random.seed(seed)
    centroids = np.zeros((k, data.shape[1]))

    # Randomly choose the first centroid.
    # Since we have no prior knowledge, choose uniformly at random
    idx = np.random.randint(data.shape[0])
    centroids[0] = data[idx, :].toarray()
    # Compute distances from the first centroid chosen to all the other data points
    distances = pairwise_distances(data, centroids[0:1], metric='euclidean').flatten()**2

    for i in range(1, k):
        # Choose the next centroid randomly, so that the probability for each data point to be chosen
        # is directly proportional to its squared distance from the nearest centroid.
        # Roughtly speaking, a new centroid should be as far as from ohter centroids as possible.
        idx = np.random.choice(data.shape[0], 1, p=distances / sum(distances))
        centroids[i] = data[idx, :].toarray()
        # Now compute distances from the centroids to all data points
        distances = np.min(pairwise_distances(data, centroids[0:i + 1], metric='euclidean')**2, axis=1)

    return centroids


def visualize_document_clusters(wiki, tf_idf, centroids, cluster_assignment, k, map_index_to_word,
                                display_content=True):
    """wiki: original dataframe
       tf_idf: data matrix, sparse matrix format
       map_index_to_word: SFrame specifying the mapping betweeen words and column indices
       display_content: if True, display 8 nearest neighbors of each centroid"""

    print('==========================================================')

    # Visualize each cluster c
    for c in range(k):
        # Cluster heading
        print('Cluster {0:d}    '.format(c)),
        # Print top 5 words with largest TF-IDF weights in the cluster
        idx = centroids[c].argsort()
        for i in range(1,6):  # Print each word along with the TF-IDF weight
            print('{0:s}:{1:.3f}'.format(map_index_to_word[idx[-i]], centroids[c, idx[-i]])),
        print('')

        if display_content:
            # Compute distances from the centroid to all data points in the cluster,
            # and compute nearest neighbors of the centroids within the cluster.
            distances = pairwise_distances(tf_idf, [centroids[c]], metric='euclidean').flatten()
            distances[cluster_assignment != c] = float('inf')  # remove non-members from consideration
            nearest_neighbors = distances.argsort()
            # For 8 nearest neighbors, print the title as well as first 180 characters of text.
            # Wrap the text at 80-character mark.
            for i in range(8):
                text = ' '.join(wiki[wiki.index == nearest_neighbors[i]]['text'].tolist()[0].split(None, 25)[0:25])
                print('\n* {0:50s} {1:.5f}\n  {2:s}\n  {3:s}'.format(wiki[wiki.index == nearest_neighbors[i]].name.tolist()[0],
                                                                     distances[nearest_neighbors[i]], text[:90],
                                                                     text[90:180] if len(text) > 90 else ''))
        print('==========================================================')


tf_idf = normalize(load_sparse_csr('data/people_wiki_tf_idf.npz'))
wiki = pandas.read_csv(open('data/people_wiki.csv'))
index_to_word = get_pickled_or_json('data/people_wiki_map_index_to_word')

# k = 3
# centroids, cluster_assignments, heterogeneity_hist = kmeans(tf_idf, k, get_initial_centroids(tf_idf, k, seed=0),
#                                                             track_heterogeneity=True, verbose=True)
# plot_heterogeneity(heterogeneity_hist, k)
# print("Converged after %d iterations!" % len(heterogeneity_hist))
# print(np.argmax(np.bincount(cluster_assignments)))


# k = 10
# heterogeneity = {}
# import time
# centroid_init_funcs = [get_initial_centroids, smart_initialize, smart_initialize_2]
# for seed in [0, 20000, 40000, 60000, 80000, 100000, 120000]:
#     print("seed=%d" % seed)
#     heterogeneity[seed] = {}
#     for centroid_init_func in centroid_init_funcs:
#         initial_centroids = centroid_init_func(tf_idf, k, seed)
#         start = time.time()
#         centroids, cluster_assignments, heterogeneity_hist, i = kmeans(tf_idf, k, initial_centroids, maxiter=400)
#         end = time.time()
#         # To save time, compute heterogeneity only once in the end
#         name = centroid_init_func.__name__
#         heterogeneity[seed][name] = compute_heterogeneity(tf_idf, k, centroids, cluster_assignments)
#         print('{0}: heterogeneity={1:.5f}, iterations={2:d}, time={3:.3f}'.format(name, heterogeneity[seed][name], i, end-start))
#         print(np.bincount(cluster_assignments))
#
# import pickle
# with open('data/heterogeneity.pickle', 'wb') as f:
#     pickle.dump(heterogeneity, f, pickle.HIGHEST_PROTOCOL)

with open('data/kmeans-arrays.npz', 'rb') as f:
    arrays = np.load(f)
    centroids = {}
    cluster_assignment = {}
    for k in [2, 10, 25, 50, 100]:
        centroids[k] = arrays['centroids_{0:d}'.format(k)]
        cluster_assignment[k] = arrays['cluster_assignment_{0:d}'.format(k)]

k = 25
visualize_document_clusters(wiki, tf_idf, centroids[k], cluster_assignment[k], k, index_to_word)
