import pandas
import sklearn
from sklearn.neighbors import NearestNeighbors
import time
from sklearn.metrics.pairwise import pairwise_distances
from nn_features_and_metrics_func import *
import numpy as np
import itertools
import copy
import matplotlib.pyplot as plt  #plotting matplotlib inline


def search_nearby_bins(query_bin_bits, table, search_radius=2):
    """
    For a given query vector and trained LSH model, return all candidate neighbors for
    the query among all bins within the given search radius.

    Example usage
    -------------
    model = train_lsh(corpus, num_vector=16, seed=143)
    q = model['bin_index_bits'][0]  # vector for the first document

    candidates = search_nearby_bins(q, model['table'])
    """
    num_vector = len(query_bin_bits.tolist()[0])
    powers_of_two = 1 << np.arange(num_vector - 1, -1, -1)

    candidate_set = set(table[query_bin_bits.dot(powers_of_two).tolist()[0]])

    for different_bits in itertools.combinations(range(num_vector), search_radius):
        # Flip the bits (n_1,n_2,...,n_r) of the query bin to produce a new bit vector.
        ## Hint: you can iterate over a tuple like a list
        alternate_bits = copy.copy(query_bin_bits)
        for i in set(different_bits):
            alternate_bits[0][i] = int(not alternate_bits[0][i])

        # Convert the new bit vector to an integer index
        nearby_bin = alternate_bits.dot(powers_of_two).tolist()[0]

        # Fetch the list of documents belonging to the bin indexed by the new bit vector.
        # Then add those documents to candidate_set
        # Make sure that the bin exists in the table!
        # Hint: update() method for sets lets you add an entire list to the set
        if nearby_bin in table:
            candidate_set.update(table[nearby_bin])

    return candidate_set


def generate_random_vectors(num_vector, dim):
    return np.random.randn(dim, num_vector)


def train_lsh(data, num_vector=16, seed=None):
    if seed is not None:
        np.random.seed(seed)

    random_vectors = np.random.randn(data.shape[1], num_vector)
    powers_of_two = 1 << np.arange(num_vector-1, -1, -1)
    bin_index_bits = (data.dot(random_vectors) >= 0)
    bin_indices = bin_index_bits.dot(powers_of_two)

    table = {}
    for data_index, bin_index in enumerate(bin_indices):
        table.setdefault(bin_index, []).append(data_index)

    model = {'data': data,
             'bin_index_bits': bin_index_bits,
             'bin_indices': bin_indices,
             'table': table,
             'random_vectors': random_vectors,
             'num_vector': num_vector}

    return model


def query(index, model, k, max_search_radius):
    data = model['data']
    table = model['table']

    # Compute bin index for the query vector, in bit representation.
    bin_index_bits = model['bin_index_bits'][index]

    # Search nearby bins and collect candidates
    candidate_set = search_nearby_bins(bin_index_bits, table, max_search_radius)

    # Sort candidates by their true distances from the query
    candidates = data[np.array(list(candidate_set)), :]
    distances = pairwise_distances(candidates, data[index], metric='cosine').flatten()
    nearest_neighbors = pandas.DataFrame({'distance': distances}, candidate_set)

    return nearest_neighbors.sort_values('distance').head(k), len(candidate_set)


num_vector = 16
wiki = pandas.read_csv(open('data/people_wiki.csv'))
index_to_word = get_pickled_or_json('data/people_wiki_map_index_to_word')
corpus = load_sparse_csr('data/people_wiki_tf_idf.npz')
model = train_lsh(corpus, num_vector=num_vector, seed=143)

# Checkpoint
# table = model['table']
# if 0 in table and table[0] == [39583] and \
#                 143 in table and table[143] == [19693, 28277, 29776, 30399]:
#     print('Passed!')
# else:
#     print('Check your code.')
# exit(0)


get_by_name = lambda name: wiki[wiki['name'] == name]

obama_name = 'Barack Obama'
obama = get_by_name(obama_name)
print(obama.index)
obama_bin = model['bin_indices'][obama.index]
print('Obama bin:', obama_bin)


# candidate_set = search_nearby_bins(model['bin_index_bits'][obama.index], model['table'], search_radius=0)
# if candidate_set == set([35817, 21426, 53937, 39426, 50261]):
#     print('Passed test')
# else:
#     print('Check your code')
#
#
# candidate_set = search_nearby_bins(model['bin_index_bits'][obama.index], model['table'], search_radius=1)
# if candidate_set == set([39426, 38155, 38412, 28444, 9757, 41631, 39207, 59050, 47773, 53937, 21426, 34547,
#                          23229, 55615, 39877, 27404, 33996, 21715, 50261, 21975, 33243, 58723, 35817, 45676,
#                          19699, 2804, 20347]):
#     print('Passed test')
# else:
#     print('Check your code')


biden = get_by_name('Joe Biden')
print(np.apply_along_axis(int, 0, model['bin_index_bits'][obama.index]).tolist())
print(np.apply_along_axis(int, 0, model['bin_index_bits'][biden.index]).tolist())

l = (model['bin_index_bits'][obama.index] == model['bin_index_bits'][biden.index]).tolist()[0]
print(sum(l))

jones = get_by_name('Wynn Normington Hugh-Jones')
l = (model['bin_index_bits'][obama.index] == model['bin_index_bits'][jones.index]).tolist()[0]
print(sum(l))

obama_neighbors_ids = model['table'][model['bin_indices'][obama.index].tolist()[0]]
obama_neighbors = wiki[wiki.index.isin(obama_neighbors_ids)]

print(obama_neighbors['name'])


def cosine_distance(x, y):
    dist = x.dot(y.T)/(np.linalg.norm(x.toarray())*np.linalg.norm(y.toarray()))
    return 1-dist[0, 0]

obama_tf_idf = corpus[obama.index, :]
biden_tf_idf = corpus[biden.index, :]

print('================= Cosine distance from Barack Obama')
print('Barack Obama - {0:24s}: {1:f}'.format('Joe Biden', cosine_distance(obama_tf_idf, biden_tf_idf)))
for doc_id in obama_neighbors_ids:
    doc_tf_idf = corpus[doc_id, :]
    print('Barack Obama - {0:24s}: {1:f}'.format(wiki[wiki.index == doc_id]['name'].tolist()[0],
                                                 cosine_distance(obama_tf_idf, doc_tf_idf)))

candidates, n = query(obama.index, model, k=10, max_search_radius=3)
print(wiki.join(candidates, how='inner')[['name', 'distance']])

num_candidates_history = []
query_time_history = []
max_distance_from_query_history = []
min_distance_from_query_history = []
average_distance_from_query_history = []
radius_to_candidates = []

for max_search_radius in range(num_vector+1):
    start = time.time()
    # Perform LSH query using Barack Obama, with max_search_radius
    result, num_candidates = query(obama.index, model, k=10,
                                   max_search_radius=max_search_radius)
    if max_search_radius > 0:
        if __name__ == '__main__':
            result = radius_to_candidates[max_search_radius-1].append(result). \
                drop_duplicates().sort_values('distance').head(10)

    radius_to_candidates.append(result)

    end = time.time()
    query_time = end - start  # Measure time

    print('Radius:', max_search_radius, query_time)
    # Display 10 nearest neighbors, along with document ID and name
    print(result.join(wiki, how='inner').sort_values('distance')[['name', 'distance']])

    # Collect statistics on 10 nearest neighbors
    average_distance_from_query = result['distance'][1:].mean()
    max_distance_from_query = result['distance'][1:].max()
    min_distance_from_query = result['distance'][1:].min()

    num_candidates_history.append(num_candidates)
    query_time_history.append(query_time)
    average_distance_from_query_history.append(average_distance_from_query)
    max_distance_from_query_history.append(max_distance_from_query)
    min_distance_from_query_history.append(min_distance_from_query)

plt.figure(figsize=(7,4.5))
plt.plot(num_candidates_history, linewidth=4)
plt.xlabel('Search radius')
plt.ylabel('# of documents searched')
plt.rcParams.update({'font.size':16})
#plt.tight_layout()

plt.figure(figsize=(7,4.5))
plt.plot(query_time_history, linewidth=4)
plt.xlabel('Search radius')
plt.ylabel('Query time (seconds)')
plt.rcParams.update({'font.size':16})
#plt.tight_layout()

plt.figure(figsize=(7,4.5))
plt.plot(average_distance_from_query_history, linewidth=4, label='Average of 10 neighbors')
plt.plot(max_distance_from_query_history, linewidth=4, label='Farthest of 10 neighbors')
plt.plot(min_distance_from_query_history, linewidth=4, label='Closest of 10 neighbors')
plt.xlabel('Search radius')
plt.ylabel('Cosine distance of neighbors')
plt.legend(loc='best', prop={'size':15})
plt.rcParams.update({'font.size':16})
#plt.tight_layout()
plt.show()

print('num_candidates_history', num_candidates_history)
print('query_time_history', query_time_history)
print('average_distance_from_query_history', average_distance_from_query_history)
print('max_distance_from_query_history', max_distance_from_query_history)
print('min_distance_from_query_history', min_distance_from_query_history)

# Quiz answers:
# What is the document id of Barack Obama's article?
# 35817
# Which bin contains Barack Obama's article? Enter its integer index.
# 50194
# Examine the bit representations of the bins containing Barack Obama and Joe Biden. In how many places do they agree?
# 14
# What was the smallest search radius that yielded the correct nearest neighbor, namely Joe Biden?
# 2
# Suppose our goal was to produce 10 approximate nearest neighbors whose average distance from the
# query document is within 0.01 of the average for the true 10 nearest neighbors. For Barack Obama,
# the true 10 nearest neighbors are on average about 0.77. What was the smallest search radius for
# Barack Obama that produced an average distance of 0.78 or better?
# 8

