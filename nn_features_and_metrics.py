import pandas
import sklearn
from sklearn.neighbors import NearestNeighbors
from nn_features_and_metrics_func import *


wiki = pandas.read_csv(open('data/people_wiki.csv'))
index_to_word = get_pickled_or_json('data/people_wiki_map_index_to_word')
word_count = load_sparse_csr('data/people_wiki_word_count.npz')
tf_idf = load_sparse_csr('data/people_wiki_tf_idf.npz')

# returns dict {word: count} per each data point
word_count_per_example = get_pickled_or_json('data/people_wiki_word_count_per_example')
# returns dict {word_tf_idf: count} per each data point
tf_idf_per_example = get_pickled_or_json('data/people_wiki_tf_idf')
print("Done loading data...")

# word_to_index = get_pickled_or_json('data/people_wiki_map_word_to_index')
# index_to_word = {int(v):k for k,v in word_to_index.items()}
# Don't use JSON dump, it converts integer keys to strings

wiki['word_count'] = word_count_per_example
wiki['tf_idf'] = tf_idf_per_example
print("Done loading and preparing data...")

get_by_name = lambda name: wiki[wiki['name'] == name]
get_word_count = lambda name: word_count[get_by_name(name).index.tolist()[0]]
get_tf_idf = lambda name: tf_idf[get_by_name(name).index.tolist()[0]]

obama_name = 'Barack Obama'
obama = get_by_name(obama_name)

model_type = NearestNeighbors(metric='euclidean', algorithm='brute')

neighbors = nearest_neighbors(model_type, tf_idf, obama.index, 10)
print(wiki.join(neighbors, how='inner').sort_values(by='distance')[['name', 'distance']])
print(top_words_tf_idf(obama, 5))

obama_tf_idf = top_words_tf_idf(obama)
# print(obama_tf_idf)

schiliro_tf_idf = top_words_tf_idf(get_by_name('Phil Schiliro'))
# print(schiliro_tf_idf)

combined_words = obama_tf_idf.merge(schiliro_tf_idf, on='word', suffixes=['_obama', '_barrio']).head(5)
# print(combined_words)

top_tf_idf = set(combined_words['word'].tolist())
wiki['has_top_tf_idf_words'] = wiki['tf_idf'].apply(lambda word_count_vector: has_top_words(top_tf_idf, word_count_vector))
print(len(wiki[wiki['has_top_tf_idf_words'] == True]))

v1, v2 = [get_tf_idf(name) for name in ['Barack Obama', 'Joe Biden']]
print(sklearn.metrics.pairwise.euclidean_distances(v1, v2))

neighbors = nearest_neighbors(model_type, word_count, obama.index, 10)
print(wiki.join(neighbors, how='inner').sort_values(by='distance')[['name', 'distance']])
print(top_words(obama, 5))


obama_words = top_words(obama)
# print(obama_words)

schiliro_words = top_words(get_by_name('Phil Schiliro'))
# print(schiliro_words)

combined_words = obama_words.merge(schiliro_words, on='word', suffixes=['_obama', '_barrio']).head(5)
print(combined_words)


top_words = set(combined_words['word'].tolist())
print(has_top_words(top_words, wiki.loc[[32]]['word_count'].tolist()[0]))
print(has_top_words(top_words, wiki.loc[[33]]['word_count'].tolist()[0]))
wiki['has_top_words'] = wiki['word_count'].apply(lambda word_count_vector: has_top_words(top_words, word_count_vector))
print(len(wiki[wiki['has_top_words'] == True]))

v1, v2, v3 = [get_word_count(name) for name in ['Barack Obama', 'George W. Bush', 'Joe Biden']]
print(sklearn.metrics.pairwise.euclidean_distances(v1, v2))
print(sklearn.metrics.pairwise.euclidean_distances(v2, v3)) # Smallest distance
print(sklearn.metrics.pairwise.euclidean_distances(v1, v3))
