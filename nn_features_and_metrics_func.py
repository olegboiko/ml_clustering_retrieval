import numpy as np
from scipy.sparse import csr_matrix
import pandas
import os
import pickle
import json


def get_pickled_or_json(name):
    pickle_name = name + '.pickle'
    json_name = name + '.json'
    if os.path.isfile(pickle_name):
        with open(pickle_name, 'rb') as f:
            return pickle.load(f)
    elif os.path.isfile(json_name):
        json_content = json.load(open(json_name))
        with open(pickle_name, 'wb') as f:
            pickle.dump(json_content, f, pickle.HIGHEST_PROTOCOL)
        return json_content


def nearest_neighbors(model, data, query_point_index, count):
    model.fit(data)
    distances, indices = model.kneighbors(data[query_point_index], n_neighbors=count)
    return pandas.DataFrame(data={'distance': distances.flatten()}, index=indices.flatten())


def load_sparse_csr(filename):
    loader = np.load(filename)
    data = loader['data']
    indices = loader['indices']
    indptr = loader['indptr']
    shape = loader['shape']

    return csr_matrix((data, indices, indptr), shape)


def unpack_dict(matrix, index_to_word):
    data = matrix.data
    indices = matrix.indices
    indptr = matrix.indptr

    num_doc = matrix.shape[0]

    return [{k: v for k, v in zip([index_to_word[word_id] for word_id in indices[indptr[i]:indptr[i + 1]]],
                                  data[indptr[i]:indptr[i + 1]].tolist())} for i in range(num_doc)]


def top_words(query_point, count=-1):
    """
    Get a table of the most frequent words in the given person's wikipedia page.
    """
    data = query_point['word_count'].tolist()[0]
    result = pandas.DataFrame(list(data.items()), columns=['word', 'count']) \
        .sort_values(by='count', ascending=False)
    return result.head(count) if count > 0 else result


def top_words_tf_idf(query_point, count=-1):
    """
    Get a table of the most frequent words in the given person's wikipedia page.
    """
    data = query_point['tf_idf'].tolist()[0]
    result = pandas.DataFrame(list(data.items()), columns=['word', 'count']) \
        .sort_values(by='count', ascending=False)
    return result.head(count) if count > 0 else result


def has_top_words(top_words, word_count_vector):
    unique_words = set(word_count_vector.keys())
    return top_words.issubset(unique_words)


def get_row(wiki, query, column="name"):
    return wiki[wiki[column] == query]
